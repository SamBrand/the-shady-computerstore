export const computers = [
    {
        id: 1,
        name: 'The Prestige',
        release: 'Release year: 1977',
        image: 'img/1.jpg',
        info: 'This bad boy will blow your mind! Ultra fast, super durable and a design gucci himself would kill for!',
        price: 14000,
    },
    {
        id: 2,
        name: "The Mindshaper",
        release: "Release year: 1998",
        image: "img/2.jpg",
        info: "If portabillity what you want, look no further! With it's 32-bit colorsceam and multiple games available, this wonder-machine will distract you for literally minutes",
        price: 3500,
    },
    {
        id: 3,
        name: "Svante",
        release: "Release year: 2019",
        image: "img/3.png",
        info: "Why do the work when Svante can do it for you? Svante can walk your dog, water your flowers and makes a mean grilled cheese sandwich! (Svante may or may not wreck your house, kill your roomate and dog, or others people. life-insurance does not apply.)",
        price: 450,
    },
]