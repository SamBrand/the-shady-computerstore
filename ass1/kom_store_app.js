import { computers } from './computers.js'

// Upper row buttons and other elements
const loan_button = document.getElementById("loan")
const loan_disp = document.getElementById("loan-disp")
const loan_amount = document.getElementById("loan-text")
const work_button = document.getElementById("work")
const income_display = document.getElementById("income")
const bank_button = document.getElementById("bank")
const bank_disp = document.getElementById("bank-display")
const buy_button = document.getElementById("buy")
const comp_selector = document.getElementById("comp-select")
const pay_back_button = document.getElementById("repay")

//Lower row buttons and other elements
const comp_name = document.getElementById("computer-name")
const comp_image = document.getElementById("image")
const comp_info = document.getElementById("computer-info")
const comp_price = document.getElementById("computer-price")
const comp_release = document.getElementById("release")
const comp_overview = document.getElementById("overview")
let selectedComp = null

//Variables
const salary = 100
let earnings = 0
let bank = 0
let loan = 0


// Setting up import for info in the computers.js file
for (const computer of computers){
    const nextComp = document.createElement("option")
    nextComp.value = computer.id
    nextComp.innerText = computer.name
    comp_selector.appendChild(nextComp)
}


//Events for buttons and banner, in total six events
work_button.addEventListener("click", function(){
    earnings = earnings + salary;
    income_display.innerText = earnings;
})

bank_button.addEventListener("click", function(){
    if (loan !== 0) {
        let payBackLoanMoney =  earnings * 0.1
        let bankingMoney = earnings * 0.9
        if (payBackLoanMoney >= loan){
            payBackLoanMoney -= loan
            bank += payBackLoanMoney + bankingMoney
            loan = 0
            earnings = 0
            updateDisplays()
            hidingPaybackButton(true)
        } else {
            loan -= payBackLoanMoney
            bank += bankingMoney
            updateDisplays()
        }
    }
    bank += earnings
    earnings = 0
    updateDisplays()
})

loan_button.addEventListener("click", function() {
    let howMuch = prompt("wadda ya need...")
    howMuch = Number(howMuch)
    if (howMuch > 2* bank) {
        return alert("nah bro, you crazy! Bank some more cash n we'll talk bout it")
    }
    loan = howMuch
    updateDisplays()
    hidingPaybackButton(false)
})

pay_back_button.addEventListener("click", function(){
    if (earnings >= loan) {
        earnings -= loan
        loan = 0
        updateDisplays()
        hidingPaybackButton(true)
    } else {
    loan -= earnings
    earnings = 0
    updateDisplays()
    }
})

buy_button.addEventListener("click", function(){
    if (bank < selectedComp.price) {
        return alert("You trippn boy?! You aint got the cash for that!")
    }
    bank -= selectedComp.price
    updateDisplays()
    if (selectedComp.id === 1){
        return alert("Great choice man! No take-backs aight? ***faint whispers*** ...What a sucker ")
    } else if (selectedComp.id === 2){
        return arlert("You gonna love the " + selectedComp.name + " it's got colors 'n everthing!")
    } else if (selectedComp.id === 3){
        return alert("The " + selectedComp.name + " is being deliverd to yo mamas house as we speak!")
    }
})

comp_selector.addEventListener("change", function(){
    if(Number(this.value) === -1){ // If no computer is selected, hide selection window
        comp_overview.style.display = 'none'
        return;
    }
    const { value: compId } = this
    selectedComp = computers.find(function(computer) {
        return Number(compId) === computer.id
    })
    comp_name.innerText = selectedComp.name
    comp_image.src = selectedComp.image
    comp_price.innerText = selectedComp.price + ' kr'
    comp_info.innerText = selectedComp.info
    comp_release.innerText = selectedComp.release
    comp_release.style.display = "block"
    comp_overview.style.display = "block"
    setFocusOnDivWithId('computer-area');
})


//Functions for easier reuse of code, in total three
function setFocusOnDivWithId(elementId) {
    const scrollIntoViewOptions = { behavior: "smooth", block: "center" };
    document.getElementById(elementId).scrollIntoView(scrollIntoViewOptions);
}

function updateDisplays(){
    income_display.innerText = earnings;
    bank_disp.innerText = bank
    loan_amount.innerText = "You owe me " + loan + " kr bro"
}

function hidingPaybackButton(state){
    if (state == true) {
        loan_disp.style.display = "none"
        loan_button.style.display = "block"
        pay_back_button.style.display = "none"
    } else {
        loan_disp.style.display = "block"
        loan_button.style.display = "none"
        pay_back_button.style.display = "block"
    }
}


